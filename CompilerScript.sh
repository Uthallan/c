
echo " "

echo "|--------------------------------------|"
echo "| Attempting to compile LogicTester.c  |"
echo "|--------------------------------------|"
echo " "

gcc LogicTester.c

echo "|--------------------------------------|"
echo "|         Compilation Results          |"
echo "|--------------------------------------|"
echo " "

echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
echo " "

./a.out

echo " "
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

echo " "
