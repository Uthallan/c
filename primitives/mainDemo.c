#include "functions.h"
#include <stdio.h>



int main()
{

int aTest[4] = {0,0,1,1};
int bTest[4] = {0,1,0,1};


printf("This is an AND gate\n\n");
printf("|a|b|out|\n");
for (int i=0; i < 4; i++)
{
  int AND_result = LOGICAL_AND(aTest[i], bTest[i]);
  printf("|%i|%i| %i |\n", aTest[i], bTest[i],AND_result);
}

printf("This is an OR gate\n\n");
printf("|a|b|out|\n");
for (int i=0; i < 4; i++)
{
  int OR_result = LOGICAL_OR(aTest[i], bTest[i]);
  printf("|%i|%i| %i |\n", aTest[i], bTest[i],OR_result);
}





}


